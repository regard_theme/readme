# Regard
<div align="center">
    <img alt="" src="./images/banner.png"/>
    <p>A high contrast colorscheme focused on code comprehension and minimizing eyes tiredness.</p>
</div>


## Palette
---
### foregroun / background
<div align="center" style="height: 150px;">
    <img alt="foreground and background color" src="./images/fgbg.png"/>
</div>

### main
<div align="center">
    <img alt="Main palette image for the regard theme." src="./images/main.png"/>
    <p>function name / keywords / types / definition & modifiers</p>
</div>

### secondary
<div align="center">
    <img alt="Main palette image for the regard theme." src="./images/secondary.png"/>
    <p>operators / strings / constants / comments</p>
</div>

## Closer Look
You can have a closer look at the theme by going to [regard's gitlab group][8] and find the repository associated to your text editor/IDE.  
If there aren't any repository that correspond, feel free to work on an implementation and contact me.

## Few Readings
### What I gathered from those
1. **Link 3** showed us using crowd-sourcing the best way to go with colors
    - High Contrast between background and foreground colors.
    - Positive text polarity (dark on light) would conceived better reading results, however since this is a dark theme, it isn't an option.  
2. From **Link 2** we understand that a color for every part of the code is way more informative and allow a faster processing. Thus, dark green is for functions' name, red for control keywords, yellow for strings, etc...  
3. **Link 5** shows no difference in code comprehension between a dark theme and a light one.  
4. The different colors where chose using **Link 4**, describing colors with red-ish/green-ish tones as the most eye catching ones.

### Bibliography
1. [Do background colors improve program comprehension in the #ifdef hell?][1]
2. [The influence of color on program readability and comprehensibility][2]
3. [Understanding the readability of colored text by crowd-sourcing on the Web][3]
4. [Which colors best catch your eyes: a subjective study of color saliency][4]
5. [The Impact of Colour Themes on Code Readability][5]

## Copyright & Licenses
All the bubble-sort programs come from the same repository, [TheRenegadeCoder/sample-programs][6].  
I used [mononoki][7] for the palettes' hexadecimal values, it's a nice monospace font.
***
[MIT](./LICENSE.md)  
[Samples](./SAMPLEPROGRAMS_LICENSE.md)  

[1]: https://doi.org/10.1007/s10664-012-9208-x
[2]: https://dl.acm.org/doi/pdf/10.1145/5600.5702
[3]: https://www.researchgate.net/profile/Giordano-Beretta/publication/228531266_Understanding_the_readability_of_colored_text_by_crowd-sourcing_on_the_Web/links/0912f5057751d83669000000/Understanding-the-readability-of-colored-text-by-crowd-sourcing-on-the-Web.pdf
[4]: https://www.academia.edu/16915583/Which_colors_best_catch_your_eyes_a_subjective_study_of_color_saliency
[5]: https://www.diva-portal.org/smash/get/diva2:1337805/FULLTEXT01.pdf
[6]: https://github.com/TheRenegadeCoder/sample-programs
[7]: https://github.com/madmalik/mononoki
[8]: https://gitlab.com/regard_theme
